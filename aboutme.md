---
layout: page
title: About me
subtitle:
---


I am Saurabh Kukade and working as software developer by profession and have masters in Computer Science.

I am mostly interested in programming but I am also interested in a great many other things. I don't know very much about any one thing but I do know a little bit about a lot of different things, and I like to think this makes me able to see connections between things that specialists might not notice.

Welcome to my blog. I write here on topics of my interest. This mainly include
Computer Science especially programming, software development. Sometimes I also like to
write about non-technical stuff.

## About My Posts ##
All the articles I write here are based upon my opinion and leaning. This is a personal blog. The opinions expressed here represent my own and not those of my employer. 

I do not expect readers to fully agree with me. I request you to evaluate ideas for yourself
without following anyone blindly and come to your own conclusions.

You are more than welcome to criticize my ideas/opinions. I expect you also to not demand me or
anyone else to respect the ideas that you find worthy of respect.
