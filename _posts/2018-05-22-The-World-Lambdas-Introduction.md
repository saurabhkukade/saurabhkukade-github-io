---
layout: post
title: The World of Lambdas
subtitle: Introduction to lambda expressions
tags: [lambda]
---

# Introduction To World Of Lambdas.<a id="sec-1" name="sec-1"></a>

Lambda expression is appearing in every other programming language and gaining a popularity
amongs the developers. So what is the lambda expression? Why should we use it? Is it just anonymous
function? what powers does it gives to programmer? Lets discuss all this in details in this series
of posts.

In this post, we will see the brief introduction to lambda expressions and how to express them in
programming language independent way.

## Introduction<a id="sec-1-1" name="sec-1-1"></a>

Lambda expressions or if I say lambda functions are anonymous functions with no names.
They only accept one input variable, with certain operations used to implement a function
with several variables.

Lambda expressions are powerfull because we can create a higher order function using them.
Here by higher order function I mean a function which can accept a another function as
argument or return a function as output.

But to able to able to create and use lambda expressions in meaningfull manner we should know
the &ldquo;Lambda Calculus&rdquo;.

Lambda Caluclus is a language of lambda terms(expressions) which is defined by certain formal
syntax and set of transformation rules, which manipulate the lambda terms.

## History<a id="sec-1-2" name="sec-1-2"></a>

-   Lambda calculus was first introduced by mathematician Alonzo Church in 1930s as part of his
    research.
-   Lambda calculus is a universal model of computation that can be simulated by any Turing Machine.

## Basics of Lambda Calculus<a id="sec-1-3" name="sec-1-3"></a>

To be able to understand the power of lambda, we must know the basics of it so that we will be
a able to form lambda expressions by our own to use in any of our favourite programming language.

### Expressing Lambda Terms<a id="sec-1-3-1" name="sec-1-3-1"></a>

Lets see how to express lambda terms in programming language independent form.
There are three forms of lambda terms.

1.  Variable
2.  Abstraction
3.  Application

Lets discuss each them in brief.

1.  Variable

    Variable can be a character or String representing a parameter or mathematical(numerical)/logical
    value.
    Example: X, 1, var, True, False

        x

        1

        True

        False

2.  Abstraction

    Abstraction is function defination. It can be thought as just constructing any other function with
    arguments.
    Example: To construct a increment function which accepts one parameter and returns adding one to it.

        (λx.x + 1)

    In above expression, before the (.) dot &ldquo;λx&rdquo; is where we are declaring an anonymous function with
    parameter &lsquo;x&rsquo; and after the (.) dot &ldquo;x+1&rdquo; is a function body where are writing a logic of a function.
    
    Please note that '+' operator used in above expression and below one also is not a lambda term. But I am using it 
    just for intuition. It can be think of as another lambda abstraction for plus operation.
    
    Another example of addtion of two numbers can be constructed as:

        (λxy.x + y)

3.  Applicattion

    Application is applying one function to an argument. Where argument is another lambda term.
    Example:

        (λx.x+1)(2)

    In above expression we are applying a variable(lambda term) &ldquo;2&rdquo; to another lambda term &ldquo;λx.x + 1&rdquo;
    which is a lambda Abstraction.

    So this is how we construct lambda expressions in three forms. Variable, Abstraction, Application.
    To summurise the inductive defination of lambda expressions with three rules which can be use to
    construct valid lambda terms:
    1.  A variable, x, is itself a valid lambda term
    2.  If &lsquo;t&rsquo; is a lambda term, and &lsquo;x&rsquo; is a variable, then "(λx.t)" is a lambda term (called a lambda abstraction);
    3.  if &lsquo;t&rsquo; and &lsquo;s&rsquo; are lambda terms, "(ts)" is a lambda term (called an application).

## Final Note<a id="sec-1-4" name="sec-1-4"></a>

This was just introduction to lambda calculus to give you an idea what lambda expressions are and
how to construct valid lambda expressions. In the next post we will discuss about free and bound
variables and evaluating a lambda terms with alpha-conversion and beta-reduction.

Please comment below for any queries, suggestions and also if you find any mistake in above post.
Thank You!
